/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet
} from 'react-native';

export default class Itens extends Component {
  render() {
    return (
   	  	<View style={styles.blocoItem}>

   	  		<View style={styles.blocoImagem}>
   	  			<Image style={{height: 100, width: 100}} source={{ uri: this.props.item.foto }} />
   	  		</View>

   	  		<View style={styles.blocoDadosItem}>
        		<Text style={styles.txtTituloItem}>{this.props.item.titulo}</Text>
        		<Text>R$ {this.props.item.valor}</Text>
        		<Text>Local: {this.props.item.local_anuncio}</Text>
        		<Text>Data: {this.props.item.data_publicacao}</Text>
        	</View>
        	
      	</View>
    );
  }
}

const styles = StyleSheet.create({

	blocoItem: {
		flexDirection: 'row',
		borderWidth: 2,
		borderColor: '#C1CDCD',
		padding: 3,
		margin: 6,
		alignItems: 'center',
		backgroundColor: '#F0FFFF'
	},

	blocoImagem: {
		marginRight: 4
	},

	blocoDadosItem: {
		flex: 1
	},

	txtTituloItem: {
		fontWeight: 'bold'
	},

});
