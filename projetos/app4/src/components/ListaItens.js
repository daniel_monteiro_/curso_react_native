/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  ScrollView
} from 'react-native';

import axios from 'axios';
import Itens from './Itens';

export default class ListaItens extends Component {

  constructor(props) {
    super(props);

    this.state = { listaItens: [] };

  }

  componentWillMount() {

    axios.get('http://faus.com.br/recursos/c/dmairr/api/itens.html').then(response => {
      
      this.setState({ listaItens: response.data });

    }).catch(() => {
      console.log("fudeu");
    });

  }

  render() {
    return (
   	  <ScrollView style={{backgroundColor: '#E0EEEE'}}>
        { this.state.listaItens.map(

          function(item) {
           return <Itens key={item.titulo} item={item}/>
          })

        }
      </ScrollView>
    );
  }
}
