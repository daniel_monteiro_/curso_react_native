/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Button
} from 'react-native';

import Topo from './src/components/topo';
import Icone from './src/components/icone';

class app3 extends Component {

  constructor(props) {
    super(props);

    this.state = { escolhaUsuario: '', escolhaComputador: '', resultado: '' };
  }

  jokenpo(e) { 

    const numAleatorio = Math.floor(Math.random() * 3);

    const escolhas = [];
    escolhas[0] = 'pedra';
    escolhas[1] = 'papel';
    escolhas[2] = 'tesoura';

    const escolhaMaquina = escolhas[numAleatorio];

    let resultado = '';
    if (escolhaMaquina === 'pedra') {
      if (e === 'pedra') {
        resultado = 'Empate';
      }

      if (e === 'papel') {
        resultado = 'Você ganhou';
      }

      if (e === 'tesoura') {
        resultado = 'Você perdeu';
      }
    }

    if (escolhaMaquina === 'papel') {
      if (e === 'pedra') {
        resultado = 'Você perdeu';
      }

      if (e === 'papel') {
        resultado = 'Empate';
      }

      if (e === 'tesoura') {
        resultado = 'Você ganhou';
      }
    }

    if (escolhaMaquina === 'tesoura') {
      if (e === 'pedra') {
        resultado = 'Você ganhou';
      }

      if (e === 'papel') {
        resultado = 'Você perdeu';
      }

      if (e === 'tesoura') {
        resultado = 'Empate';
      }
    }

    this.setState({ escolhaComputador: escolhaMaquina });
    this.setState({ escolhaUsuario: e });
    this.setState({ resultado });

  }

  render() {
    return (

      <View>
        <Topo />

        <View style={styles.painelAcoes}>
          <View style={styles.btnEscolha}>
            <Button title="pedra" onPress={() => { this.jokenpo('pedra'); }} />
          </View>

          <View style={styles.btnEscolha}>
            <Button title="papel" onPress={() => { this.jokenpo('papel'); }} />
          </View>

          <View style={styles.btnEscolha}>
            <Button title="tesoura" onPress={() => { this.jokenpo('tesoura'); }} />
          </View>

        </View>

        <View style={styles.palco}>

          <Text style={styles.txtResultado} >{this.state.resultado}</Text>

          <Icone escolha={this.state.escolhaComputador} jogador='Computador' />
          <Icone escolha={this.state.escolhaUsuario} jogador='Usuário' />

        </View>

      </View>

    );
  }
}

const styles = StyleSheet.create({
  btnEscolha: {
    width: 90
  },

  painelAcoes: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },

  palco: {
    alignItems: 'center',
    marginTop: 10
  },

  txtResultado: {
    fontSize: 25,
    fontWeight: 'bold',
    color: 'red'
  },

});

AppRegistry.registerComponent('app3', () => app3);
