import React, { Component } from 'react';
import {
  View,
  Image
} from 'react-native';

const caminhoImagem = require('../../imgs/jokenpo.png');

class Topo extends Component {

  render() {
    return (
      <View>
        <Image source={caminhoImagem} />
      </View>
    );
  }
}

export default Topo;
