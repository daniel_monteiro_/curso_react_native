import React from 'react';
import { Text, Picker, StyleSheet } from 'react-native';

export default props => (
    <Picker>
        <Picker.Item label="Soma" value="soma" />
        <Picker.Item label="Subtração" value="subtracao" />
    </Picker>
)

const styles = StyleSheet.create({
    operacao: {
        marginTop: 15,
        marginBottom: 15
    }
});