/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';

import{ Topo, Resultado, Painel } from './components';

export default props => (
  <View>
    <Topo />
    <Resultado />
    <Painel />
  </View>
);
