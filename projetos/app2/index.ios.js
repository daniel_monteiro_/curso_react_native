import React from "react";
import { Text, View, Image, AppRegistry, TouchableOpacity, Alert } from "react-native";


const Estilos = {

	principal: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	botao: {
		backgroundColor: '#538530',
		paddingVertical: 10,
		paddingHorizontal: 40,
		marginTop: 20
	},
	textoBotao: {
		color: 'white',
		fontSize: 16,
		fontWeight: 'bold'
	}

};

const gerarNovaFrase = () => {
	var numeroAleatorio = Math.floor(Math.random() * 5);

	var frases = Array();
	frases[0] = 'frase 1';
	frases[1] = 'frase 2';
	frases[2] = 'frase 3';
	frases[3] = 'frase 4';
	frases[4] = 'frase 5';


	Alert.alert(frases[numeroAleatorio]);
}

const App = () => {
  
  const { principal , botao, textoBotao} = Estilos;

  return(
  	<View style={principal}>

  		<Image source={require('./imgs/logo.png')}/>

  		<TouchableOpacity style={botao} onPress={gerarNovaFrase}>
  			<Text style={textoBotao}>Nova Frase</Text>
  		</TouchableOpacity>

    </View>
  );

};

AppRegistry.registerComponent("app2", () => App);