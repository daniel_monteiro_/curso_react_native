import React, { Component } from 'react';
import { Text, ScrollView } from 'react-native';

export default class OutrosJogos extends Component {

	render() {

		return (
			<ScrollView style={{ flex: 1, backgroundColor: '#61BD8C' }}>
				<Text style={{ flex: 1, fontSize: 20, padding: 5, marginTop: 10 }}>
					Muitos games disponíveis para smartphones e tablets contam com modos multiplayer para quem adora se divertir em partidas online. Mas quem está com um amigo em casa ou esperando para ser atendido na fila do médico também pode se distrair com jogos em portáteis. Pode parecer inédito, mas muitos títulos contam com multiplayer local.
					Você já ouviu falar desses títulos? Pois hoje nós trouxemos uma seleção com alguns dos mais divertidos games que podem ser jogados por mais de uma pessoa em um único dispositivo. Está preparado para conhecer as nossas sugestões? Então chame seus amigos e comece as disputas agora mesmo.
				</Text>
			</ScrollView>
		);
	}

}