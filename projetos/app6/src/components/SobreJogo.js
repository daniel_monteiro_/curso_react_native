import React, { Component } from 'react';
import { Text } from 'react-native';

export default class SobreJogo extends Component {

	render() {

		return (
			<Text style={{ flex: 1, fontSize: 20, padding: 5, marginTop: 10, backgroundColor: '#61BD8C' }}>
				Cara ou Coroa é um jogo simples, que consiste em se atirar uma moeda ao ar para então verificar qual de seus lados ficou voltado para cima após sua queda. É comumente utilizado para se escolher entre duas alternativas ou para resolver uma disputa entre duas partes. Esse método de escolha tem necessariamente apenas duas possibilidades de resultado, as quais têm a mesma probabilidade de ocorrência. Um uso frequente é a decisão de quem fará o movimento inicial em alguma atividade. No futebol, por exemplo, a moeda decide quem ficará com o direito de opção pelo campo ou pela bola.
			</Text>
		);
	}

}