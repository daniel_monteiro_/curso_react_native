/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image
} from 'react-native';

import { Router, Scene } from 'react-native-router-flux';

import Rotas from './src/Rotas';

export default class App extends Component {
  render() {
    return (
      <Rotas />
    );
  }
}

const styles = StyleSheet.create({
  

});