/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  StatusBar
} from 'react-native';

import { Navigator } from 'react-native-deprecated-custom-components'

import CenaPrincipal from './src/components/CenaPrincipal'
import CenaClientes from './src/components/CenaClientes'
import CenaContatos from './src/components/CenaContatos'
import CenaEmpresa from './src/components/CenaEmpresa'
import CenaServicos from './src/components/CenaServicos'


export default class app5 extends Component {
  render() {
    return (
        <Navigator 
          initialRoute={{ id: 'principal'}}

          renderScene={(route, navigator) => {

            if (route.id === 'principal') {
              return(<CenaPrincipal navigator={navigator}/>);
            }

            if (route.id === 'cliente') {
              return(<CenaClientes navigator={navigator}/>);
            }

            if (route.id === 'contato') {
              return(<CenaContatos navigator={navigator}/>);
            }

            if (route.id === 'empresa') {
              return(<CenaEmpresa navigator={navigator} />)
            }

            if (route.id === 'servico') {
              return(<CenaServicos navigator={navigator} />)
            }

          }}

        />
    );
  }
}

const styles = StyleSheet.create({
  
});

AppRegistry.registerComponent('app5', () => app5);
