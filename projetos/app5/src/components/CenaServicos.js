
import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao';

const detalheServico = require('../imgs/detalhe_servico.png');

export default class CenaServicos extends Component {
  render() {
    return (
    	<View style={{ borderWidth: 1, borderColor: 'red', flex:1, backgroundColor: 'white' }}>
        <StatusBar 
          // hidden={false}
          backgroundColor='#19D1C8'
        />

        <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#19D1C8' />

        <View style={styles.cabecalho}>
          <Image source={detalheServico} />
          <Text style={styles.txtTitulo}>Nossos Serviços</Text>
        </View>

        <View style={styles.detalheServico}>
          <Text style={styles.txtServico} >-Desenvolver Apps</Text>
          <Text style={styles.txtServico}>-Desenvolver Apps</Text>
          <Text style={styles.txtServico}>-Desenvolver Apps</Text>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({

  cabecalho: {
    flexDirection: 'row',
    marginTop: 20
  },

  txtTitulo: {
    fontSize: 30,
    color: '#19D1C8',
    marginLeft: 10,
    marginTop: 25
  },

  detalheServico: {
    marginTop: 20,
    padding: 20
  },

  txtServico: {
    fontSize: 18
  }

});
