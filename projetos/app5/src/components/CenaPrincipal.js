
import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  StyleSheet,
  TouchableHighlight
} from 'react-native';

import BarraNavegacao from './BarraNavegacao'

const logo = require('../imgs/logo.png');
const menuCliente = require('../imgs/menu_cliente.png');
const menuContato = require('../imgs/menu_contato.png');
const menuEmpresa = require('../imgs/menu_empresa.png');
const menuServico = require('../imgs/menu_servico.png');


export default class CenaPrincipal extends Component {
  render() {
    return (
    	<View style={{ borderWidth: 1, borderColor: 'red', flex:1, backgroundColor: 'white' }}>
        <StatusBar 
          // hidden={false}
          backgroundColor = '#CCC'
        />

        <BarraNavegacao />

        <View style={styles.logo}>
          <Image source={logo}/>
        </View>


        <View style={styles.menu}>
          <View style={styles.menu1}>

            <TouchableHighlight
              underlayColor={'#B9C941'}
              activityOpacity={0.3}
              onPress={() =>{
                this.props.navigator.push({ id: 'cliente' });
              }}>
              <Image style={styles.imgMenu} source={menuCliente}/>
            </TouchableHighlight>

            <TouchableHighlight
                underlayColor={'#61BD8C'}
                activityOpacity={0.3}
                onPress={() =>{
                this.props.navigator.push({ id: 'contato' });
              }}>

              <Image style={styles.imgMenu} source={menuContato}/>

            </TouchableHighlight>
          </View>


          <View style={styles.menu2}>
            <TouchableHighlight
              underlayColor={'#EC7148'}
              activityOpacity={0.3}
              onPress={() => {
                this.props.navigator.push({id: 'empresa'});
              }}
              >
              <Image style={styles.imgMenu} source={menuEmpresa}/>
            </TouchableHighlight>

            <TouchableHighlight
              underlayColor={'#19D1C8'}
              activityOpacity={0.3}
              onPress={() => {
                this.props.navigator.push({id: 'servico'})
              }}
            >
              <Image style={styles.imgMenu} source={menuServico}/>
            </TouchableHighlight>
          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  
  logo: {
    marginTop: 30,
    alignItems: 'center'
  },

  menu: {
    marginTop: 10,
    alignItems: 'center'
  },

  menu1: {
    flexDirection: 'row'
  },

  menu2: {
    flexDirection: 'row'
  },

  imgMenu: {
    margin: 15
  }


});




