
import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  Image,
  StyleSheet
} from 'react-native';

import BarraNavegacao from './BarraNavegacao'

const detalheClientes = require('../imgs/detalhe_cliente.png');
const cliente1 = require('../imgs/cliente1.png');
const cliente2 = require('../imgs/cliente2.png');

export default class CenaClientes extends Component {
  render() {
    return (
    	<View style={{ borderWidth: 1, borderColor: 'red', flex:1, backgroundColor: 'white' }}>
        <StatusBar 
          // hidden={false}
          backgroundColor='#B9C941'
        />

        <BarraNavegacao voltar navigator={this.props.navigator} corDeFundo='#B9C941' />

        <View style={styles.tituloClientes}>
          <Image source={detalheClientes} />
          <Text style={styles.txtTituloCliente}>Nossos Clientes</Text>
        </View>

        <View style={styles.subTituloClientes}>
          <Image source={cliente1} />
          <Text style={styles.txtSobreCliente}>Esse cliente é foda. Paga bem e tem vários projetos com a gente, não esqueça de dar o like.</Text>
        </View>

        <View style={styles.subTituloClientes}>
          <Image source={cliente2} />
          <Text style={styles.txtSobreCliente}>Esse cliente é foda. Paga bem e tem vários projetos com a gente, não esqueça de dar o like. (2)</Text>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  tituloClientes: {
    flexDirection: 'row',
    padding: 2,
    alignItems: 'center',
    marginTop: 10
  },

  txtTituloCliente: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#B9C941'
  },

  subTituloClientes: {
    padding: 20
  },

  barra: {
    backgroundColor: '#B9C941'
  },

  txtSobreCliente: {
    marginLeft: 15,
    color: 'blue'
  }

});




