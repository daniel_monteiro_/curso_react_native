import React from 'react'

var { Text, View, Button, AppRegistry } = require('react-native');

const gerarNumeroAleatorio = () => {
  var numero_aleatorio = Math.floor(Math.random() * 10);
  alert(numero_aleatorio)
}

const App = () => {

  return (
    <View>

      <Text> {"\n\n"}Gerador de Números Randômicos</Text>

      <Button
        title="Gerar um número randômico"
        onPress={gerarNumeroAleatorio}
      />

    </View>
  );
}

AppRegistry.registerComponent('app1', () => App);